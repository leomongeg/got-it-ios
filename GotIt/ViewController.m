//
//  ViewController.m
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 19/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import "ViewController.h"
#import "CDRTranslucentSideBar.h"
#import "DGMenuHeaderView.h"
#import "DGMenuTableViewCell.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface ViewController () <CDRTranslucentSideBarDelegate>

@property (nonatomic, strong)UIViewController* currentViewController;
@property (nonatomic, strong) CDRTranslucentSideBar *sideBar;
@property (nonatomic) Boolean isSideBarVisible;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view layoutIfNeeded];
    [_tabControl setBackgroundImage:[UIImage imageNamed:@"sinSeleccion"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_tabControl setBackgroundImage:[UIImage imageNamed:@"seleccionado"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];

    UIViewController *vc = [self viewControllerForSegmentIndex:_tabControl.selectedSegmentIndex];
    [self addChildViewController:vc];
    vc.view.frame = self.contentView.bounds;
    [self.contentView addSubview:vc.view];
    self.currentViewController = vc;
    
    
    _isSideBarVisible = NO;
    self.sideBar = [[CDRTranslucentSideBar alloc] init];
    NSLog(@"Ancho del tabbar: %f, bounds: %f ", self.view.frame.size.width, _tabControl.bounds.size.width);
    self.sideBar.sideBarWidth = (_tabControl.bounds.size.width/2) + (_tabControl.bounds.size.width/2)/2;
    self.sideBar.delegate = self;
    self.sideBar.tag = 0;
    [self.sideBar setTranslucent:NO];
    self.sideBar.translucentStyle = UIBarStyleBlack;
    [self.sideBar setTranslucentAlpha:0.9];
    [self.sideBar setTranslucentTintColor:[UIColor colorWithRed:28/255.0 green:31/255.0 blue:36/255.0 alpha:1]];
    
    
    // Create Content of SideBar
    UITableView *tableView = [[UITableView alloc] init];
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
//    v.backgroundColor = [UIColor clearColor];
    [tableView setTableHeaderView:v];
    [tableView setTableFooterView:v];
    [tableView setSeparatorColor:[UIColor colorWithRed:82/255.0 green:81/255.0 blue:81/255.0 alpha:1]];
    
    //If you create UITableViewController and set datasource or delegate to it, don't forget to add childcontroller to this viewController.
    //[[self addChildViewController: @"your view controller"];
    tableView.dataSource = self;
    tableView.delegate = self;

    // Set ContentView in SideBar
    [self.sideBar setContentViewInSideBar:tableView];
}

- (UIStatusBarStyle)preferredStatusBarStyle { return UIStatusBarStyleLightContent; }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)viewControllerForSegmentIndex:(NSInteger)index
{
    UIViewController *vc;
    switch (index) {
        case 0:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomePromotionsVC"];
            break;
        case 1:
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeStoreVC"];
            break;
    }
    
    return vc;
}

- (IBAction)showMenu:(UIButton *)sender
{
    if(_isSideBarVisible)
    {
        _isSideBarVisible = NO;
       [self.sideBar dismissAnimated:YES];
    }
    else
    {
        _isSideBarVisible = YES;
        [self.sideBar showInViewController:_currentViewController];
    }
}

- (IBAction)segmentChanged:(UISegmentedControl *)sender
{
    if (_isSideBarVisible)
    {
        _isSideBarVisible = NO;
        [self changeMenuButtonImage];
//        [sender setSelectedSegmentIndex:sender.selectedSegmentIndex == 0 ? 1 : 0];
//        return;
    }
    
//    UIViewAnimationOptionTransitionFlipFromLeft
    UIViewController *vc = [self viewControllerForSegmentIndex:sender.selectedSegmentIndex];
    [self addChildViewController:vc];
    [self transitionFromViewController:self.currentViewController toViewController:vc duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [self.currentViewController.view removeFromSuperview];
        vc.view.frame = self.contentView.bounds;
        [self.contentView addSubview:vc.view];
    } completion:^(BOOL finished) {
        [vc didMoveToParentViewController:self];
        [self.currentViewController removeFromParentViewController];
        self.currentViewController = vc;
    }];
}

- (void)changeMenuButtonImage
{
    UIImage *image = _isSideBarVisible ? [UIImage imageNamed:@"cerrarMenu"] : [UIImage imageNamed:@"menu"];
    
    [_btnMenu setImage:image forState:UIControlStateNormal];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    for (DGMenuTableViewCell *cell in ((UITableView *)self.sideBar.getContentView).visibleCells) {
        CGFloat hiddenFrameHeight = scrollView.contentOffset.y + (self.sideBar.view.bounds.size.height * 0.37) - cell.frame.origin.y;
        if (hiddenFrameHeight >= 0 || hiddenFrameHeight <= cell.frame.size.height) {
//            [self maskCellFromTop:hiddenFrameHeight withCell:cell];
            [cell maskCellFromTop:hiddenFrameHeight];
        }
    }
}

#pragma mark - CDRTranslucentSideBar

- (void)sideBar:(CDRTranslucentSideBar *)sideBar willAppear:(BOOL)animated
{
    [self changeMenuButtonImage];
    
    if(_isSideBarVisible && [_currentViewController isKindOfClass:[UITableViewController class]])
    {
        UITableView *tableView = ((UITableViewController *)_currentViewController).tableView;
        [tableView setContentOffset:tableView.contentOffset animated:NO];
        tableView.scrollEnabled = NO;
        tableView = nil;
    }
}

- (void)sideBar:(CDRTranslucentSideBar *)sideBar didDisappear:(BOOL)animated
{
    _isSideBarVisible = NO;
    if([_currentViewController isKindOfClass:[UITableViewController class]])
    {
        UITableView *tableView = ((UITableViewController *)_currentViewController).tableView;
        tableView.scrollEnabled = YES;
        tableView = nil;
    }
    
    [self changeMenuButtonImage];
}

// This is just a sample for tableview menu
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    else if (section == 1) {
        return 6;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        // StatuBar Height
        return 10;
    }
    else if (section == 1) {
        return (self.sideBar.view.bounds.size.height * 0.37);
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        UIView *clearView = [[UIView alloc] initWithFrame:CGRectZero];
//        clearView.backgroundColor = [UIColor redColor];
        return clearView;
    }
    else if (section == 1) {
//        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 44)];
        
        
        CGRect frame = CGRectMake(0, 0, tableView.bounds.size.width, (self.sideBar.view.bounds.size.height * 0.37));
        DGMenuHeaderView *headerView = (DGMenuHeaderView *)[[[NSBundle mainBundle] loadNibNamed:@"MenuHeaderPrototype" owner:self options:nil] objectAtIndex:0];
        [headerView setFrame:frame];
        headerView.backgroundColor = [UIColor clearColor];
        [headerView.lblName setText:@"Nombre Apellido"];
        [headerView.lblEmail setText:@"correo@gm.com"];
        
        
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.bounds.size.width - 15, 44)];
        UIView *separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, (self.sideBar.view.bounds.size.height * 0.37), tableView.bounds.size.width, 3.0f)];
        separatorLineView.backgroundColor = [UIColor darkGrayColor];
        [headerView addSubview:separatorLineView];
        
//        [headerView addSubview:label];
        return headerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 0;
    }
    else if (indexPath.section == 1) {
        return 44;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DGMenuTableViewCell *cell = (DGMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[DGMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        cell.tapCircleDiameter = bfPaperTableViewCell_tapCircleDiameterDefault;
        cell.backgroundColor = [UIColor clearColor];
    }
    
    if (indexPath.section == 0) {
        return cell;
    }
    else if (indexPath.section == 1) {
        
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
        
        if(indexPath.row == 0)
        {
            cell.textLabel.text      = @"Inicio";
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell.imageView setImage:[UIImage imageNamed:@"home"]];
        }else if(indexPath.row == 1)
        {
            cell.textLabel.text      = @"Promociones";
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell.imageView setImage:[UIImage imageNamed:@"promociones"]];
        }else if(indexPath.row == 2)
        {
            cell.textLabel.text      = @"Catálogo";
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell.imageView setImage:[UIImage imageNamed:@"catalogo1"]];
        }else if(indexPath.row == 2)
        {
            cell.textLabel.text      = @"Catálogo";
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell.imageView setImage:[UIImage imageNamed:@"catalogo1"]];
        }else if(indexPath.row == 3)
        {
            cell.textLabel.text      = @"Comercios";
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell.imageView setImage:[UIImage imageNamed:@"tiendas"]];
        }else if(indexPath.row == 4)
        {
            cell.textLabel.text      = @"Editar Perfil";
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell.imageView setImage:[UIImage imageNamed:@"perfil"]];
        }else if(indexPath.row == 5)
        {
            cell.textLabel.text      = @"Salir";
            cell.textLabel.textColor = [UIColor whiteColor];
            [cell.imageView setImage:[UIImage imageNamed:@"logout"]];
        }
        
//        cell.textLabel.text = [NSString stringWithFormat:@"Menu %ld", indexPath.row];
//        cell.textLabel.textColor = [UIColor whiteColor];
////        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
//        
//        [cell.imageView setImage:[UIImage imageNamed:@"menu"]];
//        return cell;
//        if (indexPath.row == 0) {
//            cell.textLabel.text = @"Menu 1";
//        }
//        else if (indexPath.row == 1) {
//            cell.textLabel.text = @"Menu 2";
//        }
//        else if (indexPath.row == 2) {
//            cell.textLabel.text = @"Menu 3";
//        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

@end

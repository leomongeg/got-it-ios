//
//  ViewController.h
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 19/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UISegmentedControl *tabControl;
@property (weak, nonatomic) IBOutlet UIView *contentView;



- (IBAction)showMenu:(UIButton *)sender;
- (IBAction)segmentChanged:(UISegmentedControl *)sender;

@end


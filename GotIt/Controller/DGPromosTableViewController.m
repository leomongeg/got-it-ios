//
//  DGPromosTableViewController.m
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 19/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import "DGPromosTableViewController.h"
#import "DGPromosTableViewCell.h"

@interface DGPromosTableViewController ()

@property (nonatomic, strong) NSArray *data;

@end

@implementation DGPromosTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    _data = @[
              @{ @"name":@"Tienda 1", @"image": @"arthurStreet", @"location": @"Centro comercial del sur"},
              @{ @"name":@"Tienda 2", @"image": @"tiendaPepeson", @"location": @"Multi Plaza Escazú"},
              @{ @"name":@"Tienda 3", @"image": @"zapaticoRoto", @"location": @"Multi Plaza del Este"},
              @{ @"name":@"Tienda 4", @"image": @"arthurStreet", @"location": @"Centro comercial del sur"},
              @{ @"name":@"Tienda 5", @"image": @"tiendaPepeson", @"location": @"Multi Plaza Escazú"},
              @{ @"name":@"Tienda 6", @"image": @"zapaticoRoto", @"location": @"Multi Plaza del Este"},
              @{ @"name":@"Tienda 7", @"image": @"arthurStreet", @"location": @"Centro comercial del sur"},
              @{ @"name":@"Tienda 8", @"image": @"tiendaPepeson", @"location": @"Multi Plaza Escazú"},
              @{ @"name":@"Tienda 9", @"image": @"zapaticoRoto", @"location": @"Multi Plaza del Este"}
              ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_data count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DGPromosTableViewCell *cell = (DGPromosTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"PromosCell" forIndexPath:indexPath];
 
    if(cell)
    {
        NSDictionary *info = [_data objectAtIndex:indexPath.row];
        [cell.backgroundImage setImage:[UIImage imageNamed:[info objectForKey:@"image"]]];
        [cell.lblStoreName setText:[info objectForKey:@"name"]];
        [cell.lblStoreLocation setText:[info objectForKey:@"location"]];
        
        cell.usesSmartColor = YES;
        cell.tapCircleDiameter = bfPaperTableViewCell_tapCircleDiameterDefault;
        cell.textLabel.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier: @"ViewPromotionProducts" sender: self];
    NSLog(@"Row seleccionado: %ld", indexPath.row);
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

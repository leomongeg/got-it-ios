//
//  DGPromotionsViewController.m
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 19/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import "DGPromotionsViewController.h"

@interface DGPromotionsViewController ()

@end

@implementation DGPromotionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

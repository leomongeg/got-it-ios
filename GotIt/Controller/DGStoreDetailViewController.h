//
//  DGStoreDetailViewController.h
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 19/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DGStoreDetailViewController : UIViewController
- (IBAction)goBack:(id)sender;

@end

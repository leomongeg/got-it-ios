//
//  DGProductsCollectionViewController.h
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 10/11/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSAlwaysOnTopHeader.h"

@interface DGProductsCollectionViewController : UICollectionViewController<LMActionsHandlerProtocol>

- (IBAction)goBack:(UIButton *)sender;

@end

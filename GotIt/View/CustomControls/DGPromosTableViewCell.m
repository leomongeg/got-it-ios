//
//  DGPromosTableViewCell.m
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 19/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import "DGPromosTableViewCell.h"

@implementation DGPromosTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
//    [_backgroundImage removeFromSuperview];
//    [self addSubview:_backgroundImage];
//    [self sendSubviewToBack:_backgroundImage];

    [self bringSublayerToFront:[self.contentView.layer.sublayers objectAtIndex:0]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) bringSublayerToFront:(CALayer *)layer
{
    [layer removeFromSuperlayer];
    [self.contentView.layer insertSublayer:layer atIndex:(int)[self.contentView.layer.sublayers count]];
}

@end

//
//  DGProductsCollectionViewCell.h
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 10/11/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DGProductsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end

//
//  CSAlwaysOnTopHeader.h
//  CSStickyHeaderFlowLayoutDemo
//
//  Created by James Tang on 6/4/14.
//  Copyright (c) 2014 Jamz Tang. All rights reserved.
//

#import "CSCell.h"

@protocol LMActionsHandlerProtocol;

@interface CSAlwaysOnTopHeader : CSCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) id<LMActionsHandlerProtocol> delegate;
- (IBAction)back:(UIButton *)sender;

@end


@protocol LMActionsHandlerProtocol <NSObject>

-(IBAction)goBack:(UIButton*)sender;

@end
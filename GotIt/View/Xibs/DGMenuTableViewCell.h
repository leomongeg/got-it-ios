//
//  DGMenuTableViewCell.h
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 21/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BFPaperTableViewCell.h"

@interface DGMenuTableViewCell : BFPaperTableViewCell

- (void)maskCellFromTop:(CGFloat)margin;
- (CAGradientLayer *)visibilityMaskWithLocation:(CGFloat)location;

@end

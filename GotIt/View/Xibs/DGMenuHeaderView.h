//
//  DGMenuHeaderView.h
//  GotIt
//
//  Created by Jorge Leonardo Monge García on 20/10/14.
//  Copyright (c) 2014 DigitalParadox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DGMenuHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;

@end
